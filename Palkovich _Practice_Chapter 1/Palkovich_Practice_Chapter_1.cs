﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Palkovich__Practice_Chapter_1
{
    class Palkovich_Practice_Chapter_1
    {
        static void Main(string[] args)
        {
            HelloWorld hi = new HelloWorld();
            hi.World();

            Banner cool = new Banner();
            cool.CreateBanner();

            VisualX marksthespot = new VisualX();
            marksthespot.DrawX();
        }
    }
}
