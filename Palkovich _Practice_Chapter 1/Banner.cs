﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Palkovich__Practice_Chapter_1
{
    class Banner
    {
        public Banner()
        {

        }

        public void CreateBanner()
        {
            Console.Clear();
            Console.WriteLine("\n\n");
            Console.WriteLine("*********************************************************");
            Console.WriteLine("\t**\tDeveloper:Amy Palkovich\t\t**\t");
            Console.WriteLine("*********************************************************");
            Console.WriteLine("\n\n");
            Console.WriteLine("Press Enter to Continue.");
            Console.ReadKey();

        }

    }
}
